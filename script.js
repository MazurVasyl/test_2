let arr = [2, 5, 55, 8, 100, -5];

let sum = arr.reduce((total, value) => {
  return total + value;
});

console.log(sum);